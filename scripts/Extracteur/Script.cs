﻿using System;

// Space Engineers DLLs
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using VRageMath;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using SpaceEngineers.Game.ModAPI.Ingame;

/*
 * Must be unique per each script project.
 * Prevents collisions of multiple `class Program` declarations.
 * Will be used to detect the ingame script region, whose name is the same.
 */
namespace Extracteur {

/*
 * Do not change this declaration because this is the game requirement.
 */
public sealed class Program : MyGridProgram {
    #region Extracteur

    int pistonDownFlag;

    float vitesse_descente_piston;
    float vitesse_monte_piston;
    float vitesse_rotor;

    IMyTextPanel ecran; 
    IMyMotorAdvancedStator rotor;
    IMyPistonBase piston_1;
    IMyPistonBase piston_2;
    IMyPistonBase piston_3;
    IMyPistonBase piston_4;
    IMyPistonBase piston_5;

    IMyShipDrill drill_1;
    IMyShipDrill drill_2;
    IMyShipDrill drill_3;
    IMyShipDrill drill_4;
    IMyShipDrill drill_5;

    public Program() {
        Runtime.UpdateFrequency = UpdateFrequency.Update100;

        pistonDownFlag = 0;

        vitesse_descente_piston = -0.02f;
        vitesse_monte_piston = -1f;
        vitesse_rotor = 10;

        ecran = GridTerminalSystem.GetBlockWithName("Wide LCD Panel") as IMyTextPanel; 
        rotor = GridTerminalSystem.GetBlockWithName("Advanced Rotor") as IMyMotorAdvancedStator;
        piston_1 = GridTerminalSystem.GetBlockWithName("Piston 1") as IMyPistonBase;
        piston_2 = GridTerminalSystem.GetBlockWithName("Piston 2") as IMyPistonBase;
        piston_3 = GridTerminalSystem.GetBlockWithName("Piston 3") as IMyPistonBase;
        piston_4 = GridTerminalSystem.GetBlockWithName("Piston 4") as IMyPistonBase;
        piston_5 = GridTerminalSystem.GetBlockWithName("Piston 5") as IMyPistonBase;

        rotor.TargetVelocityRPM = 0;
        piston_1.Velocity = 0;
        piston_2.Velocity = 0;
        piston_3.Velocity = 0;
        piston_4.Velocity = 0;
        piston_5.Velocity = 0;

        drill_1 = GridTerminalSystem.GetBlockWithName("Drill 1") as IMyShipDrill;
        drill_2 = GridTerminalSystem.GetBlockWithName("Drill 2") as IMyShipDrill;
        drill_3 = GridTerminalSystem.GetBlockWithName("Drill 3") as IMyShipDrill;
        drill_4 = GridTerminalSystem.GetBlockWithName("Drill 4") as IMyShipDrill;
        drill_5 = GridTerminalSystem.GetBlockWithName("Drill 5") as IMyShipDrill;

        
        drill_1.ApplyAction("OnOff_Off");
        drill_2.ApplyAction("OnOff_Off");
        drill_3.ApplyAction("OnOff_Off");
        drill_4.ApplyAction("OnOff_Off");
        drill_5.ApplyAction("OnOff_Off");

    }

    public void Save() {}

    public void Main(string argument, UpdateType updateSource) {

        if(argument == "1"){
            pistonDownFlag = 1;
        }
        if(argument == "2"){
            pistonDownFlag = -1;
        }

        ecran.WriteText(
            "piston 1 : " + piston_1.CurrentPosition.ToString()
        + "\npiston 2 : " + piston_2.CurrentPosition.ToString()
        + "\npiston 3 : " + piston_3.CurrentPosition.ToString()
        + "\npiston 4 : " + piston_4.CurrentPosition.ToString()
        + "\npiston 5 : " + piston_5.CurrentPosition.ToString()
        );

        if(pistonDownFlag == 1 && piston_1.CurrentPosition >= 9.5 && piston_2.CurrentPosition >= 9.5 && piston_3.CurrentPosition >= 9.5 && piston_4.CurrentPosition >= 9.5 && piston_5.CurrentPosition >= 9.5){
            pistonDownFlag = -1;
        }

        if(pistonDownFlag == 1){
            drill_1.ApplyAction("OnOff_On");
            drill_2.ApplyAction("OnOff_On");
            drill_3.ApplyAction("OnOff_On");
            drill_4.ApplyAction("OnOff_On");
            drill_5.ApplyAction("OnOff_On");
            
            rotor.TargetVelocityRPM = vitesse_rotor;
            
            piston_1.Velocity = vitesse_descente_piston;
            piston_2.Velocity = vitesse_descente_piston;
            piston_3.Velocity = vitesse_descente_piston;
            piston_4.Velocity = vitesse_descente_piston;
            piston_5.Velocity = vitesse_descente_piston;

            piston_1.ApplyAction("Extend");
            piston_2.ApplyAction("Extend");
            piston_3.ApplyAction("Extend");
            piston_4.ApplyAction("Extend");
            piston_5.ApplyAction("Extend");
        }else if(pistonDownFlag == -1){
            piston_1.ApplyAction("Retract");
            piston_2.ApplyAction("Retract");
            piston_3.ApplyAction("Retract");
            piston_4.ApplyAction("Retract");
            piston_5.ApplyAction("Retract");

            piston_1.Velocity = vitesse_monte_piston;
            piston_2.Velocity = vitesse_monte_piston;
            piston_3.Velocity = vitesse_monte_piston;
            piston_4.Velocity = vitesse_monte_piston;
            piston_5.Velocity = vitesse_monte_piston;
            
            rotor.TargetVelocityRPM = 0;

            drill_1.ApplyAction("OnOff_Off");
            drill_2.ApplyAction("OnOff_Off");
            drill_3.ApplyAction("OnOff_Off");
            drill_4.ApplyAction("OnOff_Off");
            drill_5.ApplyAction("OnOff_Off");
        }
    }

    #endregion // Extracteur
}}
